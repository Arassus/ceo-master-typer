﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IssueListController : MonoBehaviour
{
    public List<Button> IssueObjects = new List<Button>();

    public List<Issue> IssueEntities = new List<Issue>();
    
    int IssueEntityCount = 0;

    public Transform Content;

    public Transform IssueControlPrefab;

    public Transform IssueDescriptionTab;

    private void OnEnable()
    {
        RefreshAllIssues();
    }

    private void OnDisable() 
    {
        DestroyAllIssues();
    }

    private void Update() 
    {
        if(Time.deltaTime > 0f)
        {
            if(IssueEntityCount != IssueEntities.Count)
            {
                RefreshAllIssues();
            }  
        }
    }

    public void RefreshAllIssues()
    {
        DestroyAllIssues();

        LoadIssues();

        RenderIssues();
    }

    public void DestroyAllIssues()
    {
        for(int i =0; i<IssueObjects.Count; i++)
        {
            Destroy(IssueObjects[i].gameObject);
        }

        IssueObjects.Clear();
    }

    public void LoadIssues()
    {
        IssueEntities = DataController.Issues;
            
        IssueEntityCount = IssueEntities.Count;
    }

    public void RenderIssues()
    {
        int childCount = Content.childCount;
        
        float NewY = Config.ui.UseButton_InitialY;

        bool IssueDescriptionShouldBeChecked = true;

        foreach(Issue issue in IssueEntities)
        {
            Transform IssueControl = Instantiate(IssueControlPrefab,Content);

            IssueControl.name = "Issue Button : " + issue.Name;

            Button IssueButton = IssueControl.GetComponent<Button>();

            IssueObjects.Add(IssueButton);

            IssueButton.onClick.AddListener( () => {LoadIssueDescription(issue);} );

            Text IssueText = IssueControl.GetComponentInChildren<Text>();

            IssueText.text = issue.Name;

            IssueControl.Translate(0, NewY, 0, Space.Self);

            NewY -= Config.ui.UseButton_AdditionalY;

            if(IssueDescriptionTab.gameObject.activeInHierarchy)
            {
                IssueDescriptionTabController controller = IssueDescriptionTab.GetComponent<IssueDescriptionTabController>();

                if(issue.Name == controller.Name.text && issue.StartDate.ToString() == controller.Date.text)
                {
                    IssueDescriptionShouldBeChecked = false;
                }
            }
        }

        if(IssueDescriptionShouldBeChecked)
        {
            IssueDescriptionTab.gameObject.SetActive(false);
        }
    }

    public void LoadIssueDescription(Issue ChosenIssue)
    {
        IssueDescriptionTabController controller = IssueDescriptionTab.GetComponent<IssueDescriptionTabController>();

        if(controller.Name.text == ChosenIssue.Name && controller.Date.text == ChosenIssue.StartDate.ToString() )
        {
            IssueDescriptionTab.gameObject.SetActive(false);

            controller.Name.text = Config.issue.DefaultName;
            
            controller.Date.text = Config.issue.DefaultStartDate;
            
            controller.DeadLine.text = Config.issue.DefaultDeadline;
            
            controller.Description.text = Config.issue.DefaultDescription;
        }
        else
        {
            IssueDescriptionTab.gameObject.SetActive(true);

            controller.Name.text = ChosenIssue.Name;
            
            controller.Date.text = ChosenIssue.StartDate.ToString();
            
            controller.DeadLine.text = "Deadline : " + ChosenIssue.Deadline.ToString();
            
            controller.Description.text = ChosenIssue.Description;
        }
    }
}

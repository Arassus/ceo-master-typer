﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public static class DataController 
{
	public static List<Issue> Issues = new List<Issue>();

	public static List<Issue> GeneratedIssues = new List<Issue>();

	public static void UpdateIssues()
	{
		if(GeneratedIssues.Count < 10 && Issues.Count < 10)
		{
			GeneratedIssues.Add(GenerateNewIssue());
		}

		 

		// Log.Send("Checking Generated Issues . . .");

		for(int i=0;i<GeneratedIssues.Count;i++)
		{
			int result = System.DateTime.Compare(TimeController.InGameDateTime, GeneratedIssues[i].StartDate);

			if(result >= 0)
			{
				// Log.Send("\t\tIssue Has Started");
 
				Issues.Add(GeneratedIssues[i]);

				GeneratedIssues.Remove(GeneratedIssues[i]);
			}

			if(result < 0)
			{
				// Log.Send("\t\tIssue Has Not Started Yet");
			}
		}



		// Log.Send("Checking Issues . . .");

		for(int i=0;i<Issues.Count;i++)
		{

			int result = System.DateTime.Compare(TimeController.InGameDateTime, Issues[i].Deadline);

			if(result > 0)
			{
				// Log.Send("\t\tIssue is Past Due");

				Issues.Remove(Issues[i]);
			}

			if(result == 0)
			{
				// Log.Send("\t\tIssue Is Due Today");
			}

			if(result < 0)
			{
				// Log.Send("\t\tIssue Due Later");
			}
		}
	}	

	public static Issue GenerateNewIssue()
	{
		System.DateTime StartDate = StartInSomeTime();

		Issue GeneratedIssue = new Issue(ReadLineFromTxtFile(Config.data.FirstNameFilePath, Config.data.GenerateFirstNameId()), Config.data.GenerateIssueDescription(), StartDate.AddDays(Config.data.GenerateDaysUntilDeadline()),Random.Range(1,30), StartDate);

		return GeneratedIssue;
	}

	[MenuItem("Tools/Read file")]
	public static string ReadLineFromTxtFile(string FilePath, int LineId)
	{
		StreamReader Reader = new StreamReader(FilePath,System.Text.Encoding.UTF8);

		string[] Lines = File.ReadAllLines(FilePath, System.Text.Encoding.UTF8);

		string Line = Lines[LineId];

		return Line;
	}

	private static System.DateTime StartInSomeTime()
	{
		System.TimeSpan AddedTime = new System.TimeSpan(Random.Range(0,3),Random.Range(0,14),Random.Range(0,14),Random.Range(0,14));

		System.DateTime StartAt = TimeController.InGameDateTime.Add(AddedTime);

		return StartAt;
	}
}

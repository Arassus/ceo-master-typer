﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeButtonController : MonoBehaviour
{
    public UnityEngine.UI.Text TimeText;

	void Start ()
    {
		
	}

	void Update ()
    {
        TimeText.text = TimeController.InGameDateTime.ToString("HH:mm:ss dd.MM.yy");
    }
}

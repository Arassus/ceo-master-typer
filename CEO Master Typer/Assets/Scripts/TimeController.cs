﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class TimeController
{
    //public static int Second, Minute, Hour, Day, Month, Year;
    public static DateTime InGameDateTime;

    public static float SecondLenght;

    private static float PassedTime = 0f;

    public static void Start(System.DateTime dateTime, float SecondLenght)
    {
        TimeController.InGameDateTime = dateTime;

        TimeController.SecondLenght = SecondLenght;
    }

    public static void Update()
    {
        if(Time.deltaTime > 0f)
        TimeController.InGameDateTime = TimeController.InGameDateTime.AddSeconds(Time.deltaTime/SecondLenght);
    }
}

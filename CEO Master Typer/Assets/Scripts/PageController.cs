using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageController : MonoBehaviour 
{
	public List<Button> Buttons = new List<Button>();

	private void OnEnable() 
	{
		Debug.Log("Starting Page Controller");

		for(int i=0; i<transform.childCount; i++)
		{
			Button AquiredFromChild = transform.GetChild(i).GetComponent<Button>();

			if(AquiredFromChild == null)
			{
				Debug.Log("NULL");
			}
			else
			{
				Debug.Log("+");
				
				Buttons.Add(AquiredFromChild);
			}
		}
	}

	
}

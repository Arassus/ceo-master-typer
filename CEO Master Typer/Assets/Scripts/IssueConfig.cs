﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IssueConfig 
{
	public string DefaultName = "Default Issue";

	public string DefaultDescription = "Default Description";

	public string DefaultDeadline = "Default DeadLine";

	public string DefaultStartDate = "Default StartDate";

	public string DefaultStoryPoints = "Default StoryPoints";
}

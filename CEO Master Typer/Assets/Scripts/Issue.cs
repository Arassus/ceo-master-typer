﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Issue
{
    public string Name;

    public string Description;

    public DateTime Deadline;

    public int StoryPoints;

    public DateTime StartDate;
 
    public Issue(string Name, string Description, DateTime Deadline, int StoryPoints, DateTime StartDate)
    {
        this.Name = Name;

        this.Description = Description;

        this.StoryPoints = StoryPoints;

        this.StartDate = StartDate;

        this.Deadline = Deadline;
    }

    public override string ToString()
    {
        return "Issue : " + Name + " : " + Description + " : " + Deadline.ToString() + " : " + StoryPoints.ToString();
    }
}
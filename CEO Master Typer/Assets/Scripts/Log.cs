﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;

public static class Log
{
	public static string LogName = "";
	
	public static string LogPath = "";

	public enum Mode
	{
		Neutral,
		Test,
		Success,
		Failure,
		Assign
	}

	public static void Send(string Message)
	{
		Send(Message, Mode.Neutral);
	}

    [MenuItem("Tools/Write file")]
	public static void Send(string Message, Mode LogMode)
	{
		if(LogName == "")
        {
			LogName = "Log " + DateTime.Now.ToString("yyyy_MM_dd hh_mm_ss");
			LogPath = "Assets/Log/" + LogName + ".txt";
		}
		switch(LogMode) 
		{
			case Mode.Neutral:
 			Debug.Log(Config.logger.NeutralMessageColor(Message));
			break;
			
			case Mode.Test:
			Debug.Log(Config.logger.TestMessageColor(Message));
			break;
			
			case Mode.Success:
			Debug.Log(Config.logger.SuccessMessageColor(Message));
			break;
			
			case Mode.Failure:
			Debug.Log(Config.logger.FailureMessageColor(Message));
			break;
			
			case Mode.Assign:
			Debug.Log(Config.logger.AssignMessageColor(Message));
			break;

			default:
			Debug.Log(Config.logger.NeutralMessageColor(Message));
			break;
		}

        StreamWriter writer = new StreamWriter(LogPath, true);
        writer.WriteLine(DateTime.Now.ToString() + " : " + Message);
        writer.Close();
	}
}

public class LogConfig 
{
	public string NeutralMessageColor(string Message)
	{
		return "<color=black>" + Message + "</color>";
	}

	public string TestMessageColor(string Message)
	{
		return "<color=yellow>" + Message + "</color>";
	}

	public string SuccessMessageColor(string Message)
	{
		return "<color=lime>" + Message + "</color>";
	}

	public string FailureMessageColor(string Message)
	{
		return "<color=red>" + Message + "</color>";
	}

	public string AssignMessageColor(string Message)
	{
		return "<color=magenta>" + Message + "</color>";
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Config 
{
	public static LogConfig logger = new LogConfig();

	public static DataConfig data = new DataConfig();

	public static UIConfig ui = new UIConfig();

	public static IssueConfig issue = new IssueConfig();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SystemController : MonoBehaviour
{
    public DateTime InitialDate;

    public float SecondLenght = 0.2f;

    private void OnEnable()
    {
        InitialDate = DateTime.Now;
    }

    private void Start()
    {
        TimeController.Start(InitialDate, SecondLenght);
    }

    private void Update()
    {
        if(TimeController.SecondLenght != this.SecondLenght)
        TimeController.SecondLenght = this.SecondLenght;

        TimeController.Update();

        DataController.UpdateIssues();
    }
}

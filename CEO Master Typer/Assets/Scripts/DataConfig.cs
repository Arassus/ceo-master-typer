﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataConfig
{
	public string FirstNameFilePath = "Assets/Data/imiona.txt"; 

	public int GenerateFirstNameId()
	{
		return Random.Range(0,1024);
	}
	

	public string LastNameFilePath = "Assets/Data/imiona.txt"; 

	public int GenerateLastNameId()
	{
		return Random.Range(0,1024);
	}
	

	public string DictionaryFilePath = "Assets/Data/imiona.txt"; 

	public int GenerateWordId()
	{
		return Random.Range(0,1024);
	}


	public string GenerateIssueDescription()
	{		
		return "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
	}


	public int GenerateDaysUntilDeadline()
	{
		return Random.Range(1,5);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuTabController : MonoBehaviour 
{
	public GameObject IssueTab, IssueDescription;

	public Button IssueButton;

    public Button[] IssueTabDeactivationButtons;

	public void ToggleActiveIssueTab()
	{
		IssueTab.SetActive(!IssueTab.activeInHierarchy);
        
        if(IssueDescription.activeInHierarchy)
            IssueDescription.SetActive(false);
	}

    public void DeactivateIssueTab()
    {
        if(IssueTab.activeInHierarchy)
            IssueTab.SetActive(false);
        
        if(IssueDescription.activeInHierarchy)
            IssueDescription.SetActive(false);
    }

	private void Start() 
	{
        IssueButton.onClick.AddListener(ToggleActiveIssueTab);

        foreach(Button button in IssueTabDeactivationButtons)
        {
            button.onClick.AddListener(DeactivateIssueTab);
        }
	}
}
